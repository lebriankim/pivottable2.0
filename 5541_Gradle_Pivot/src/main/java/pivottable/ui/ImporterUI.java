package pivottable.ui;

import java.io.File;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import pivottable.App;
import pivottable.core.AppState;
import pivottable.io.Importer;
import pivottable.io.ImporterType;
import pivottable.io.PHDImporter;
import pivottable.model.PivotTable;

/**
 * JavaFX view for the Importer dialog
 */
public class ImporterUI {

	/* Class attributes */

	private App app;
	private Stage primaryStage;
	private Scene importerScene;
	private PivotTable pt;
	private Importer dataImporter;

	/* Constructors */

	/**
	 * ImporterUI Constructor
	 */
	public ImporterUI(  App app ) {
		this.app = app;
		this.primaryStage = app.getRootStage();
		this.pt = app.getPivotTable();
	}

	/* Class methods */

	/**
	 * Defines and draws all the controls for the Importer Dialog
	 */
	public void drawUI() {

		// Set dialog box title
		primaryStage.setTitle( "Select data source input" );

		// Input selector combo box
		ComboBox<String> inputSelector = new ComboBox<String>();
		inputSelector.getItems().addAll( "CSV file", "Database", "PivotHero File" );
		inputSelector.setPromptText( "Select the data source input" );

		// CSV path
		Label csvPathLabel = new Label( "Path to CSV file:" );
		TextField csvPath = new TextField();
		csvPath.setMinWidth( 500 );
		String sep = File.separator;
		csvPath.setText( "" );

		// Browse for CSV button
		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Browse for CSV data source" );
		fileChooser.getExtensionFilters()
		.addAll( new ExtensionFilter( "CSV files", "*.csv" ) );
		Button browse = new Button( "Browse" );
		try {
			browse.setOnAction( e -> {
				try {
					String text = fileChooser.showOpenDialog( primaryStage ).getPath();
					csvPath.setText( text );
				} catch ( Exception e2 ) {}  // when the user clicks cancel from browse, do nothing

			} );
		} catch ( Exception e2 ) {
			System.out.println( "check selection" );
		}

		// Path
		Label dbPathLabel = new Label( "Server/Database:" );
		TextField dbAddress = new TextField( "" );
		dbAddress.setMinWidth( 300 );

		// Username
		Label usernameLabel = new Label( "Username:" );
		TextField username = new TextField();
		username.setMinWidth( 300 );

		// Password
		Label passwordLabel = new Label( "Password:" );
		PasswordField password = new PasswordField();
		password.setMinWidth( 300 );
		password.setText( "" );

		// SQL Select Statement
		Label sqlLabel = new Label( "Enter a valid \nSQL statement: " );
		TextArea sql = new TextArea();
		sql.setMinSize(300, 80);
		sql.setText( "" );

		// Connected to DB Label
		Label connectLabel = new Label("You are now connected to: " + dbAddress.getText() );

		// Submit Button
		Button submit = new Button();
		submit.setText( "Submit" );
		submit.setOnAction( e -> {
			if ( inputSelector.getValue() == "CSV file" ) {
				dataImporter = app.getImporterFactory().getImporter( ImporterType.CSV );
				// Checks to see if CSV file path is valid
				if (dataImporter.validCnx(csvPath.getText())) {
					try {
						// Create PivotTableUI
						dataImporter.importData( csvPath.getText(), pt );
						app.setState( AppState.PIVOTTABLE );
					} catch ( Exception e1 ) {
						// Display error message for 	
						Alert alert = new Alert(AlertType.ERROR);
						alert.setHeaderText("CSV File Loading Error");
						alert.setContentText("There was an error trying to load the CSV file."
								+ "\nPlease try again or contact Support.");
						alert.showAndWait();
					}	
				}
				// Else, display pop up error message for CSV file path error
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Invalid CSV File Path Error");
					alert.setContentText("Please check your file path.");
					alert.showAndWait();
				}
			}
			if ( inputSelector.getValue() == "Database" ) {
				// Compose connection string
				String cnx = "jdbc:mysql://" + dbAddress.getText() + "##" + username.getText() + "##"
						+ password.getText() + "##" + sql.getText();
				// Attempt to create PivotTableUI using cnx
				try {
					// Create PivotTableUI
					if(dataImporter.importData(cnx, pt)){
						//dataImporter.importData(cnx, pt);
						app.setState( AppState.PIVOTTABLE );
					}

				} catch (Exception e1) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Data Extraction Error");
					alert.setContentText("There was an error while trying to "
							+ "extract data from the Server/Database.\nPlease"
							+ "try again.");
					alert.showAndWait();
				}

			}
		} );
		submit.setVisible(false);

		// Create grid pane for CSV controls
		GridPane csvGrid = new GridPane();
		csvGrid.setHgap( 5 );
		csvGrid.setVgap( 5 );
		csvGrid.add( browse, 2, 0 );
		csvGrid.add( csvPathLabel, 0, 0 );
		csvGrid.add( csvPath, 1, 0 );
		csvGrid.setVisible( false );

		// Create grid pane for db URL and credentials controls
		GridPane dbGrid = new GridPane();
		Button connect = new Button();
		dbGrid.setHgap( 5 );
		dbGrid.setVgap( 5 );
		dbGrid.add( dbPathLabel, 0, 0 );
		dbGrid.add( dbAddress, 1, 0 );
		dbGrid.add( usernameLabel, 0, 1 );
		dbGrid.add( username, 1, 1 );
		dbGrid.add( passwordLabel, 0, 2 );
		dbGrid.add( password, 1, 2 );
		dbGrid.add( connect, 2, 2 );
		dbGrid.setVisible( false );

		// Create grid pane for SQL statement
		GridPane sqlStmt = new GridPane();
		sqlStmt.setHgap( 5 );
		sqlStmt.setVgap( 5 );
		sqlStmt.add( sqlLabel, 0, 3 );
		sqlStmt.add( sql, 1, 3 );
		sqlStmt.setVisible(false);

		// Create grid pane for valid db connection
		GridPane validDBConnect = new GridPane();
		validDBConnect.setHgap( 5 );
		validDBConnect.setVgap( 5 );
		validDBConnect.add(connectLabel, 0, 0);
		validDBConnect.setVisible(false);

		// DB Connect Button
		connect.setText("Connect");
		connect.setOnAction( e -> {

			if ( inputSelector.getValue() == "Database" && 
					!dbAddress.getText().isEmpty() && 
					!username.getText().isEmpty() && 
					!password.getText().isEmpty() ) {
				dataImporter = app.getImporterFactory().getImporter( ImporterType.MYSQL );
				// Compose connection string
				String cnx = "jdbc:mysql://" + dbAddress.getText() + "##"
						+ username.getText() + "##" + password.getText();
				// Check if valid connection using cnx
				if (dataImporter.validCnx(cnx)) {
					dbGrid.setVisible(false);
					connect.setVisible(false);
					validDBConnect.setVisible(true);
					sqlStmt.setVisible(true);
					submit.setVisible(true);
				} 
				// If not valid connection using cnx, display error pop-up
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Connection Fail Error");
					alert.setContentText("Please check your database URL "
							+ "and credentials.");
					alert.showAndWait();
				}
			}
			else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setHeaderText("Incomplete connection details");
				alert.setContentText("Please check your database URL "
						+ "and credentials.");
				alert.showAndWait();
			}
		});
		connect.setVisible(false);


		// Vertical Selector Box
		VBox selBox = new VBox();
		selBox.setSpacing( 15 );
		selBox.setPadding( new Insets( 20, 20, 20, 20 ) );
		selBox.getChildren().addAll( inputSelector, csvGrid, dbGrid, validDBConnect, sqlStmt, submit );

		// Put the scene together and display it
		importerScene = new Scene( selBox, 700, 400 );
		importerScene.getStylesheets().add("application.css");

		primaryStage.setScene( importerScene );
		primaryStage.centerOnScreen();
		primaryStage.show();
		primaryStage.setOnCloseRequest( e -> System.exit( 0 ) );

		// Listen for imputSelector changes
		inputSelector.getSelectionModel().selectedItemProperty()
		.addListener( (v, oldValue, newValue) -> {
			if ( newValue == "CSV file" ) {
				validDBConnect.setVisible(false);
				sqlStmt.setVisible(false);
				dbGrid.setVisible( false );
				csvGrid.setVisible( true );
				submit.setVisible(true);
				connect.setVisible(false);
			}
			if ( newValue == "Database" ) {
				submit.setVisible(false);
				validDBConnect.setVisible(false);
				sqlStmt.setVisible(false);
				csvGrid.setVisible( false );
				dbGrid.setVisible( true );
				connect.setVisible(true);
			}
			if ( newValue == "PivotHero File" ) {
				submit.setVisible(false);
				validDBConnect.setVisible(false);
				sqlStmt.setVisible(false);
				csvGrid.setVisible( false );
				dbGrid.setVisible( false );
				connect.setVisible(false);
				PHDImporter phdImporter = new PHDImporter(this.app);
				phdImporter.drawUI();
			}
		} );

	}

}
