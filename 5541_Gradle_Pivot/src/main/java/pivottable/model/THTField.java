package pivottable.model;

import java.io.Serializable;
import java.util.HashSet;

/**
 * A structure used as the data element in THTNode
 */
public class THTField implements Serializable {

    /* For serializing */
    private static final long serialVersionUID = 5873230486544542563L;
    
    /* Class Attributes */

    private String fieldName;
    private HashSet<Integer> fieldIds;

    /* Constructor */

    /**
     * The THTField contains a field name used for labeling the PivotTable and a
     * set of fieldIds corresponding to the field value to be used in
     * calculating PivotCell values.
     * 
     * @param fieldName
     * @param fieldIds
     */
    public THTField( String fieldName, HashSet<Integer> fieldIds ) {
        this.fieldName = fieldName;
        this.fieldIds = fieldIds;
    }

    /* Getter and setter methods */

    /**
     * Gets the THTField's name
     * 
     * @return The field name
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Gets the THTField's ids
     * 
     * @return The set of IDs for the field value
     */
    public HashSet<Integer> getFieldIds() {
        return fieldIds;
    }

}