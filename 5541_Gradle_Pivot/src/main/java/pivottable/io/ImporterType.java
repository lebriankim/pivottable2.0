package pivottable.io;


public enum ImporterType {
    MYSQL, SQLSERVER, CSV
}
