package pivottable.ui;

import java.util.ArrayList;

import pivottable.model.PTFieldSet;
import pivottable.model.PTFieldValue;
import pivottable.model.PTOperation;
import pivottable.model.PivotTable;

/**
 * A controller for the Pivot Table
 */
public class PTController {

    /* Class attributes */

    /**
     * Reference to the model instance
     */
    private PivotTable model;

    /**
     * Referene to the view instance
     */
    private PTViewInterface view;

    /* Constructors */

    /**
     * The PTController constructor takes a model and view instance
     * 
     * @param model
     * @param view
     */
    public PTController( PivotTable model, PTViewInterface view ) {
        this.model = model;
        this.view = view;
    }

    /* Class Methods */

    /**
     * Sets Aggregation Field parameter
     * @param fieldIndex
     */
    public void setAggregationField(int fieldIndex) {
        model.setAggregationField( fieldIndex );
    }

    /**
     * Sets Column Fields parameter
     * @param columnFields
     */
    public void setColumnFields(ArrayList<PTFieldSet> columnFields) {
        model.setColumnFields( columnFields );
    }

    /**
     * Sets Row Fields parameter
     * @param rowFields
     */
    public void setRowFields(ArrayList<PTFieldSet> rowFields) {
        model.setRowFields( rowFields );
    }

    /**
     * Sets Operation Type parameter
     * @param operation
     */
    public void setOperationType(PTOperation operation) {
        model.setOperationType( operation );
    }

    /**
     * Sets Filters
     */
    public void setFilterFieldValues(ArrayList<PTFieldValue> filterFieldValues) {
        model.setFilterFieldValues(filterFieldValues);
    }
    
    /**
     * Sets ColumnsPerPage
     */
    public void setColsPerPage(int colsPerPage){
        model.setColsPerPage( colsPerPage );
    }
    
    /**
     * Sets RowsPerPage
     */
    public void setRowsPerPage(int rowsPerPage){
        model.setRowsPerPage( rowsPerPage );
    }
    
    /**
     * Sets CurrentPage
     */
    public void setCurrentPage (int currentPage){
        model.setCurrentPage( currentPage );
    }
    
    /**
     * Refreshes the table cells without rebuilding the header trees
     */
    public void refreshCells() {
        model.refreshCells();
    }
    
    
    /**
     * Draws the pivot table based on the paging parameters
     */
    public void drawPage() {
        model.pivotTableInit();
        view.drawPivotTable();
    }

}
