package pivottable.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pivottable.App;
import pivottable.core.AppState;
import pivottable.model.PivotTable;

public class PHDImporter {

	private App app;
	private Stage primaryStage;

	public PHDImporter( App app ) {
		this.app = app;
		primaryStage = app.getRootStage();
	}

	public void loadPHD(String filePath) {

		try {
			FileInputStream fileIn = new FileInputStream(filePath);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			PivotTable pt = (PivotTable) in.readObject();
			in.close();
			fileIn.close();
			app.setPivotTable( pt );
			app.setPivotInit(true);
			app.setState( AppState.PIVOTTABLE );    
			app.setPivotInit(true); // must be set a second time to allow PDF export once data is restored from PHD
		}catch(IOException i) {
			// Display error message 	
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Error");
			alert.setContentText(i.getLocalizedMessage());
			alert.showAndWait();
			return;
		}catch(ClassNotFoundException c) {
			// Display error message 	
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Error");
			alert.setContentText(c.getLocalizedMessage());
			alert.showAndWait();
			return;
		}

	}

	public void drawUI() {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Choose a file to load" );
		// Set extension filter
		FileChooser.ExtensionFilter extFilter =
				new FileChooser.ExtensionFilter( "PHD files (*.phd)", "*.phd" );
		fileChooser.getExtensionFilters().add( extFilter );

		// Show save file dialog
		File file = fileChooser.showOpenDialog( primaryStage );

		if ( file != null ) {
			loadPHD( file.getAbsolutePath() );
		} 

	}

}
