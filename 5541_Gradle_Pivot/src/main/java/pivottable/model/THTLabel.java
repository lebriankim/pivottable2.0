package pivottable.model;

import java.io.Serializable;

/**
 * The THTLabel helps in the construction of nested headers labels by storing
 * the label and the number of columns or rows it should span.
 */
public class THTLabel implements Serializable {

    /* For serializing */
    private static final long serialVersionUID = 5172716248711538354L;
    
    /* Class attributes */

    private String label;
    private int span;

    /* Constructor */

    /**
     * The THTLabel stores the label for a field and the number of columns or
     * rows it spans based on the column or row segmenting.
     * 
     * @param label The field value to use for the header label
     * @param span The number of columns or rows the label should span
     */
    public THTLabel( String label, int span ) {
        this.label = label;
        this.span = span;
    }
    
    /* Getter and setter methods */

    /**
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return The number of columns or rows the label should span in the header
     */
    public int getSpan() {
        return span;
    }

}