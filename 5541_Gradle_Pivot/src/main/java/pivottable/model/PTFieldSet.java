package pivottable.model;

import java.io.Serializable;

/**
 * Class for holding the field selections 
 *
 */
public class PTFieldSet implements Serializable {
    
    /* For serializing */
    private static final long serialVersionUID = -7861696268984918653L;

    /** Holds a reference to the fieldIndex **/
    private int fieldIndex;
    
    /** Sort order for use in tree construction **/
    private PTSortOrder sortOrder;

    public PTFieldSet(int fieldIndex, PTSortOrder sortOrder) {
        this.fieldIndex = fieldIndex;
        this.sortOrder = sortOrder;
    }

    /**
     * @return the fieldIndex
     */
    public int getFieldIndex() {
        return fieldIndex;
    }

    /**
     * @return the sortOrder
     */
    public PTSortOrder getSortOrder() {
        return sortOrder;
    }
    
    /**
     * Equals
     */
    @Override
    public boolean equals(Object other){
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (getClass() != other.getClass())
            return false;
        PTFieldSet o2 = (PTFieldSet)other;
        if (fieldIndex != o2.fieldIndex)
            return false;
        if (sortOrder != o2.sortOrder)
            return false;
        return true;
    }

}
