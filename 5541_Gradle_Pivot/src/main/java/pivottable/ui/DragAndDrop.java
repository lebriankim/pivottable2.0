package pivottable.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DragAndDrop extends Application {

	private int counter = 0;
	private final static ObjectProperty<ListCell<String>> dragSource = new SimpleObjectProperty<>();
	static boolean listDest = false;

	/** 
	 * Populate a listView with random items
	 * @param listView
	 */
	public void populate(ListView<String> listView){
		for (int i=0; i<5; i++ ) {
			listView.getItems().add("Item "+(++counter));
		}
	}

	/**
	 * set a placeholder into a listView
	 * @param listView
	 */
	public static void placeholder(ListView<String> listView){
		listView.getItems().add("Drag fields here");
	}


	/**
	 * dragDrop between listViews 
	 * will handle any 2 listViews taking only one of them as parameter
	 * @param listView
	 */
	public static void dragDrop(ListView<String> listView) {

		listView.setCellFactory(lv -> {
			ListCell<String> cell = new ListCell<String>(){
				@Override
				public void updateItem(String item , boolean empty) {
					super.updateItem(item, empty);
					setText(item);
				}
			};

			cell.setOnDragDetected(event -> {
				listDest = false;
				if (! cell.isEmpty() && ! cell.getItem().contains("Drag fields here")) {
					Dragboard db = cell.startDragAndDrop(TransferMode.MOVE);
					ClipboardContent cc = new ClipboardContent();
					cc.putString(cell.getItem());
					db.setContent(cc);
					dragSource.set(cell);
				}
			});

			cell.setOnDragOver(event -> {
				Dragboard db = event.getDragboard();
				if (db.hasString()) {
					event.acceptTransferModes(TransferMode.MOVE);
				}
			});

			cell.setOnDragDone(event -> {
				if (listDest) listView.getItems().remove(cell.getItem()); //need to avoid removal when dropped on wrong place
				if (listView.getItems().size() == 0){
					listView.getItems().add("Drag fields here");
				}
			});

			cell.setOnDragDropped(event -> {
				Dragboard db = event.getDragboard();
				if (db.hasString() && dragSource.get() != null) {
					ListCell<String> dragSourceCell = dragSource.get();
					listView.getItems().add(dragSourceCell.getItem());
					listView.getItems().remove("Drag fields here");
					event.setDropCompleted(true);
					dragSource.set(null);
					listDest = true;
				} else {
					event.setDropCompleted(false);
					listDest = false;
				}
			});

			return cell ;
		});

	} 

	// a testing stage
	@Override
	public void start(Stage primaryStage) {
		primaryStage.show();
		ListView<String> list1 = new ListView<>();
		ListView<String> list2 = new ListView<>();

		populate(list1);
		placeholder(list2);
		dragDrop(list1);
		dragDrop(list2);

		VBox selBox = new VBox();
		selBox.setSpacing(20);
		selBox.setPadding(new Insets(20, 20, 20, 20));
		selBox.getChildren().addAll(list1, list2);

		//put the scene together and display it
		Scene scene = new Scene(selBox, 600, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(e -> Platform.exit());
	}

	// run the testing stage
	public static void main(String[] args) {
		launch(args);

	}
}