# PivotHero
Version 1.3

## Team
Eugen Caruntu, Michael Durcak, Juyoun Kim, Joe Plazak, Abrahim Wadhai

# Description

PivotHero is a stand-alone data-summarization software tool. It allows users to import datasets from CSV files or via selected database connections, perform summarization analyses, and save and export these analyses in the form of printable files and PivotHeroDocument (.phd) files.

The application can be run from the standalone executable PivotHero v1.3.jar found at the root of this delivery.

# Project Structure

```
The build path for the project is 5541_Gradle_Pivot/src/main/java

pivottable/                 # Main package
    App.java                # The main entry for the application

pivottable.core/            # Utility package
    AppState.java           # Enumeration class for different application states

pivottable.io/              # Package for importing and exporting
    Importer.java           # Importer interface
    CSVImporter.java        # Imports CSV file and initializes PivotTable data
    MysqlImporter.java      # Imports from Mysql db and initializes PivotTable data
    MSsqlImporter.java      # Imports from MS SQL Server and initializes PivotTable data
    PTFileWriter.java       # Utility to write to file
    PDFExporter.java        # Exports to pdf
    PHDExporter.java        # Exports to .phd file (Serialization of PivotTable)
    PHDImporter.java        # Imports .phd file (dreplaces App PivotTable instance)
    ImporterFactory.java    # Factory that returns Importer based on ImporterType
    ImporterType.java       # Enumeration that specifies importers supported by application
    
pivottable.model/           # Package containing main and helper model classes
    PivotTable.java         # Class that represents a pivot table. Contains 
                            # schema, data-structures that contain header 
                            # information, and a collection of cells 
                            # representing the table
    PTCell.java             # Represents individual table cell. Performs aggregation calculations
    PTData.java             # Class that represents the raw dataset imported
                            # by the Importer and data-structures that index the 
                            # data by field and record id
    PTDataType.java         # Enumeration used for defining Field datatypes
    PTField.java            # Class to store the field name, datatype, and 
                            # record id index by field value
    PTFieldSet.java         # Datatype to hold the schema selections for rowFields, colFields
    PTFieldValue.java       # Datatype to hold schema specification for field and sort order
    PTOperation.java        # Enumeration used for defining aggregation 
                            # operations
    PTSortOrder.java        # Enumeration for defining sort order
    TableHeaderTree.java    # A general-tree structure used to create the
                            # header structure for a given pivot table's row 
                            # or column fields
    THTNode.java            # A TableHeaderTree node
    THTField.java           # The data-element stored in a TableHeaderTree node
                            # that represents the node's field value and ids
                            # of records that have that field value
    THTLabel.java           # Helps in the construction of nested headers labels 
                            # by storing the label and the number of columns or 
                            # rows it should span.
                            
pivottable.ui/              # package that contains view and controller classes
    ConsolePTView.java      # Implementation of PTView for the console
    DragAndDrop.java        # JavaFx helper class
    ImporterUI.java         # JavaFx view/controller for the Importer dialog box
    JfxPTView.java          # JavaFx view for the table
    PivotTableUI.java       # JavaFx view/controller for the PivotTable 
    PTController.java       # Controller for the PivotTable (Needs to be refactored, 
                            # pattern not properly implemented)
    PTViewInterface.java    # Interface for PivotTable views
```


## Compiling and running the application

### The application can be run from the standalone executable ```PivotHero v1.3.jar``` found at the root of this delivery.

### Using the gradle wrapper

1. In a terminal navigate to the ```5541_Gradle_Pivot``` folder
2. Run the application by entering:
    ```./gradlew run``` on a mac or
    ```gradlew run``` on Windows

### Using Eclipse

1. Build the dependencies using gradle either using the buildship plugin for eclipse or manually in the command line ```gradle build``` (Note, it might take 5-15 seconds to build using the wrapper)

2. Run pivottable/App.java

### Using included csvs

After launching the application, select "CSV file" from the "Select Data Source" dropdown control in the first dialog box. Navigate to ```5541_Gradle_Pivot/src/main/java/files``` to use one of the included datasets.

#### Included Datasets

The files folder in the source project includes two datasets:

1. SalesJan2009.csv (Sample Sales Data) source: https://support.spatialkey.com/spatialkey-sample-csv-data/

2. 0354004-eng.csv (Computer Service Industry Statistics) source: http://open.canada.ca/data/en/dataset/1493bea7-3eb8-4dcc-86e7-002226ec77ba