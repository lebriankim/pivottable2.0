package pivottable.model;

/**
 * ENUM for operations name: Sum, Average, Count, etc. We should use this
 * anytime we pass numbers for some kind of code, for example:
 * 
 * calculate(Operation.SUM, 5, 8);
 * 
 * public void calculate(int operation, int x, int y){
 *     if (operation == Operation.SUM) { 
 *         add(x,y); 
 *     }
 * }
 * 
 */
public enum PTOperation {
    
    /* Enumeration values */
    
    SUM, COUNT, MEAN, MINIMUM, MAXIMUM;
}
