package pivottable.model;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Enumeration
 */
public enum PTDataType {
    
    /* Enumeration values */
    
    DATETIME(
            "^(\\d{1,2}|\\d{4})(-|\\/)(\\d{1,2})(-|\\/)(\\d{1,2}|\\d{4})(T|\\s)(\\d{2}:\\d{2}:\\d{2})$"),
    DATE("^(\\d{1,2}|\\d{4})(-|\\/)(\\d{1,2}|\\d{4})(-|\\/)(\\d{4})$"),
    EMAIL("^\\w+@\\w+(\\.).*$"), DOUBLE("^(-{0,1})(\\d+)(\\.)(\\d+)$"),
    INT("^(-{0,1})(\\d+)$"), STRING("^.*$");

    /* Class attributes */
    
    private final Predicate<String> tester;

    /* Constructors */
    
    private PTDataType( String regexp ) {
        tester = Pattern.compile( regexp ).asPredicate();
    }

    /* Class Methods */
    
    /**
     * Calculates the appropriate value for an array of fieldValues
     * @param fieldValues
     * @return
     */
    public static Optional<PTDataType> getTypeOfField(String[] fieldValues) {
        return Arrays.stream( values() )
                .filter( dt -> Arrays.stream( fieldValues ).allMatch( dt.tester ) )
                .findFirst();
    }

}
