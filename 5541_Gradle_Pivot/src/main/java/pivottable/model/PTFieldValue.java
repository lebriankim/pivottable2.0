package pivottable.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Helper class that holds a fieldIndex and fieldValue for filtering
 */
public class PTFieldValue implements Serializable {
    
    /* For serializing */
    
    private static final long serialVersionUID = 6175618047734374790L;
    
    /* Class attributes */   
    
    private int fieldIndex;
    private String[] fieldValues;

    /* Constructors */
    
    public PTFieldValue(int fieldIndex, String[] fieldValues) {
        this.fieldIndex = fieldIndex;
        this.fieldValues = fieldValues;
    }
    
    /* Class Methods */

    /**
     * @return the fieldIndex
     */
    public int getFieldIndex() {
        return fieldIndex;
    }

    /**
     * @return the fieldValues
     */
    public String[] getFieldValues() {
        return fieldValues;
    }

    /**
     * Equals
     */
    @Override
    public boolean equals(Object other){
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (getClass() != other.getClass())
            return false;
        PTFieldValue o2 = (PTFieldValue)other;
        if (!Arrays.equals(fieldValues, o2.fieldValues))
            return false;
        if (fieldIndex != o2.fieldIndex)
            return false;
        return true;
    }
    
}
