package pivottable.ui;

import java.util.ArrayList;

import pivottable.model.PTFieldSet;
import pivottable.model.PivotTable;
import pivottable.model.THTLabel;

/**
 * ConsolePTView draws a pivot table to the console.
 */
public class ConsolePTView implements PTViewInterface {

    /* Class constants */

    /**
     * Sets the number of characters per column
     */
    private static final int COLWIDTH = 25;

    /* Class attributes */

    private PivotTable model;

    /* Constructors */

    /**
     * ConsolePTView takes a PivotTable instance.
     * 
     * @param model The PivotTable model
     */
    public ConsolePTView( PivotTable model ) {
        this.model = model;
    }

    /**
     * Draws the PivotTable to the console.
     */
    @Override
    public void drawPivotTable() {
        
        int colStart = model.getPageColStart();
        int colSpan = model.getColsPerPage();
        int rowStart = model.getPageRowStart();
        int rowSpan = model.getRowsPerPage();
        
        /*
         * Outputs the current parameters for the pivot table
         */
        printParams();

        System.out.println();

        /*
         * Transform row headers into table that include spans for segments
         */
        ArrayList<ArrayList<String>> rowHeaderTable = createRowHeaderTable();

        /*
         * For a console-based output, the column headers need to be drawn
         * first. The left-hand side of the table will have rows, so to align
         * the column-headers properly, the shift must be calculated.
         */
        int rowLabelWidth = rowHeaderTable.size() * COLWIDTH;

        int colMax = model.getTableColSize(); // max width
        // we go out of bounds if colStart+colSpan > colMax
        // if out of bounds, span should be colMax - colStart
        int cSpan = 0;
        if ( colStart + colSpan > colMax ) {
            cSpan = colMax - colStart;
        }
        else {
            cSpan = colSpan;
        }

        // Turn header structure into array
        String[][] colHeaderArray =
                new String[model.getColHeadStruct().size() - 1][colMax];

        // Create array for header
        for ( int i = 1; i < model.getColHeadStruct().size(); i++ ) {

            int rowCount = 0;

            for ( THTLabel label : model.getColHeadStruct().get( i ) ) {

                for ( int j = 0; j < label.getSpan(); j++ ) {
                    if ( j == 0 ) {
                        colHeaderArray[i - 1][rowCount] = label.getLabel();
                    }
                    else {
                        colHeaderArray[i - 1][rowCount] = "";
                    }
                    rowCount++;
                }
            }
        }

        // Print the header table for the column fields and segments
        String s = "| %-" + (COLWIDTH - 3) + "s ";
        String blankRowWidth = "%" + rowLabelWidth + "s";

        // number of ROWS in column header
        for ( int i = 0; i < colHeaderArray.length; i++ ) {
            System.out.printf( blankRowWidth, "" );
            // check first label for row
            if ( colHeaderArray[i][colStart] == "" ) {
                // label is blank, go backwards through array to get label
                int newJ = colStart;
                while ( (colHeaderArray[i][newJ] == "") && (newJ - 1 >= 0) ) {
                    newJ--;
                }
                System.out.print( String.format( s, colHeaderArray[i][newJ] )
                        .substring( 0, COLWIDTH ) );
            }
            else {
                System.out.print( String.format( s, colHeaderArray[i][colStart] )
                        .substring( 0, COLWIDTH ) );
            }
            // for the other labels
            for ( int j = colStart + 1; j < colStart + cSpan; j++ ) {
                System.out.print( String.format( s, colHeaderArray[i][j] ).substring( 0,
                        COLWIDTH ) );
            }
            System.out.println(); // New line
        }

        StringBuilder line = new StringBuilder();
        int charSpan = (cSpan * (COLWIDTH)) + (rowLabelWidth);

        for ( int i = 0; i < charSpan; i++ ) {
            line.append('-');
        }

        System.out.println( line );

        // Print Row Headers and Rows
        int rowMax = model.getTableRowSize(); // max width
        // we go out of bounds if rowStart+rowSpan > rowMax
        // if out of bounds, span should be rowMax - rowStart
        int rSpan = 0;
        if ( rowStart + rowSpan > rowMax ) {
            rSpan = rowMax - rowStart;
        }
        else {
            rSpan = rowSpan;
        }

        for ( int i = rowStart; i < rSpan + rowStart; i++ ) {
            // Print row labels
            for ( int j = 0; j < rowHeaderTable.size(); j++ ) {
                if ( i == rowStart ) {
                    // check first label for
                    if ( rowHeaderTable.get( j ).get( i ).charAt( 0 ) == ' ' ) {
                        // label is blank, go backwards through array to get
                        // label
                        int newI = i;
                        while ( (rowHeaderTable.get( j ).get( newI ).charAt( 0 ) == ' ')
                                && (newI - 1 >= 0) ) {
                            newI--;
                        }
                        System.out.print( rowHeaderTable.get( j ).get( newI )
                                .substring( 0, COLWIDTH ) );
                    }
                    else {
                        System.out.print( rowHeaderTable.get( j ).get( i ).substring( 0,
                                COLWIDTH ) );
                    }
                }
                else {
                    System.out.print(
                            rowHeaderTable.get( j ).get( i ).substring( 0, COLWIDTH ) );
                }
            }

            // Print rows of PivotCells
            for ( int k = colStart; k < cSpan + colStart; k++ ) {
                System.out.printf( s, model.getCellAt( i, k ) );
            }
            System.out.println(); // New line
        }
        System.out.println( line );
        System.out.println();

    }

    /**
     * Outputs the params for the pivot table
     */
    private void printParams() {
        // Column Params
        String columnFields = "";
        int count = 1;
        for ( PTFieldSet field : model.getColumnFields() ) {
            columnFields += model.getPTData().getFieldName( field.getFieldIndex() );
            if ( count++ < model.getColumnFields().size() ) {
                columnFields += ", ";
            }
        }
        // Row Params
        String rowFields = "";
        count = 1;
        for ( PTFieldSet field : model.getRowFields() ) {
            rowFields += model.getPTData().getFieldName( field.getFieldIndex() );
            if ( count++ < model.getRowFields().size() ) {
                rowFields += ", ";
            }
        }
        // Aggregation Field
        String aggregationField =
                model.getPTData().getFieldName( model.getAggregationField() );
        // Operation

        String preformatted = "%-" + COLWIDTH + "s: %s";

        // Print the output
        System.out
                .println( String.format( preformatted, "Column Fields", columnFields ) );
        System.out.println( String.format( preformatted, "Row Fields", rowFields ) );
        System.out.println(
                String.format( preformatted, "Aggregation Field", aggregationField ) );
        System.out.println( String.format( preformatted, "Operation",
                model.getOperationType().name() ) );
    }

    /**
     * Creates the structure needed to display the header table
     * 
     * @return 2D ArrayList containing row labels with spans
     */
    private ArrayList<ArrayList<String>> createRowHeaderTable() {

        ArrayList<ArrayList<String>> rowHeaderTable = new ArrayList<>();

        String s = "%-" + COLWIDTH + "s";

        // For each row (rowHeadStruct is 1-indexed / doesn't show root)
        for ( int i = 1; i < model.getRowHeadStruct().size(); i++ ) {

            // Instantiate new ArrayList to hold row
            rowHeaderTable.add( new ArrayList<String>() );

            // For each label in the row
            for ( THTLabel label : model.getRowHeadStruct().get( i ) ) {

                // Format the label for consistent spacing

                String formatted = String.format( s, label.getLabel() );

                /*
                 * Push the formatted label to the row. Uses i - 1 since
                 * rowHeaderArrayList is 0 indexed
                 */
                rowHeaderTable.get( i - 1 ).add( formatted );

                // Get the row span for the label
                int span = label.getSpan();

                // Fill the remaining span with whitespace
                for ( int j = 1; j < span; j++ ) {
                    formatted = String.format( s, "" );
                    // Uses i - 1 since rowHeaderArrayList is 0 indexed
                    rowHeaderTable.get( i - 1 ).add( formatted );
                }
            }
        }
        return rowHeaderTable;
    }

}
