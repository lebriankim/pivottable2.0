package pivottable.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * The pivot table header section for both row and column parameters requires a
 * general tree structure to manage its segmentation. This structure is also
 * crucial in the building of the pivot table, providing the recordIds used by
 * pivot cells in their aggregate value calculation.
 */
public class TableHeaderTree implements Serializable {

    /* For Serializing */
    private static final long serialVersionUID = 7373723866549787141L;
    
    /* Class Attributes */

    private THTNode root;
    private int height = 0;

    /* Constructor */

    /**
     * Creates new TableHeaderTree
     */
    public TableHeaderTree() {
        this.root = new THTNode( new THTField( "Root", new HashSet<Integer>() ), null,
                new ArrayList<THTNode>() );
    }

    /* Class Methods */

    public THTNode getRoot() {
        return this.root;
    }

    /**
     * Builds the tree from a ColumnFields or RowFields set and PivotData
     * reference. As the tree is built, each node's set of IDs is created by
     * intersecting the field's IDs with its parent. As such, the root node's
     * IDs are the full set of IDs.
     * 
     * @param fieldSet Set of column or row fields
     * @param pivotData Reference to pivotData
     */
    public void buildTree(ArrayList<PTFieldSet> fieldSet, PivotTable pivotTable) {

        // Get reference to PTData
        PTData ptData = pivotTable.getPTData();

        // Used to track depth in tree
        int depth = 0;

        // Set height of the tree
        height = fieldSet.size();

        // Start by creating hash set of all IDs from the records key-set
        HashSet<Integer> rootIds = new HashSet<Integer>( ptData.getRecords().keySet() );

        // Check filters
        if ( pivotTable.getFilterFieldValues().size() > 0 ) {
            // For each filter field, intersect rootIds with filter id set
            for ( PTFieldValue fieldValue : pivotTable.getFilterFieldValues() ) {
                HashSet<Integer> filterIds = new HashSet<>();
                for (String field : fieldValue.getFieldValues() ){
                    // Create union of fieldValue ids
                    filterIds.addAll( ptData.getFieldIdsByField( fieldValue.getFieldIndex(), field ) );
                }
                rootIds = ptData.getIntersection( rootIds, filterIds );
            }
        }

        // Get the root node of the tree
        THTNode root = getRoot();

        // Initialize the root element with rootIds
        root.setElement( new THTField( "ROOT", rootIds ) );

        // Initialize the recursive adding of tree nodes using the root as
        // parent
        addNodeR( root, fieldSet, ptData, depth, getHeight() );

    }

    /**
     * Recursive method that takes parent node, field set, pivotData reference,
     * depth, and height parameters and builds the header tree structure
     * 
     * @param parent
     * @param fieldSet
     * @param pivotData
     * @param depth
     * @param height
     */
    private void addNodeR(THTNode parent, ArrayList<PTFieldSet> fieldSet,
            PTData pivotData, int depth, int height) {

        /*
         * The recursive end condition: if the current depth is the same as the
         * height of the tree then we're at the leaf nodes
         */
        if ( depth == height ) {
            return;
        }

        /*
         * Otherwise get the field values for the current level of the tree:
         * pivotData.getFieldValuesAlpha( fieldSet.get(depth).getFieldIndex() ).
         * 
         * For example, fieldset.get(0).getFieldIndex() gets the IDs for the
         * root level of the tree. Subsequent levels represent segmenting of the
         * fields in the level above. To keep a running tab of the IDs for the
         * node (and to possibly calculate total columns at a later time), the
         * intersection of the fields for the element are intersected with the
         * parent's id set. If this intersection is 0, skip the row; otherwise,
         * create a THTField object that has the field value's name and the
         * intersected set of IDs calculated previously.
         */
        for ( String field : pivotData.getFieldValuesAlpha(
                fieldSet.get( depth ).getFieldIndex(),
                fieldSet.get( depth ).getSortOrder() ) ) {

            // Get intersection of child IDs with parent
            HashSet<Integer> ids =
                    pivotData.getIntersection( parent.getElement().getFieldIds(),
                            fieldSet.get( depth ).getFieldIndex(), field );

            // If intersection is empty, skip -- will produce row or column of
            // 0's
            if ( ids.isEmpty() )
                continue;

            // Add a new node for the field value to the tree and save node
            // reference as current
            THTNode current = this.addNode( new THTField( field, ids ), parent );

            // finally do the recursive step using depth+1 for depth and current
            // as the parent node
            addNodeR( current, fieldSet, pivotData, depth + 1, height );
        }

    }

    /**
     * Adds new Node to the tree
     * 
     * @param element
     * @param parent
     * @return
     */
    private THTNode addNode(THTField element, THTNode parent) {
        THTNode newNode = new THTNode( element, parent, new ArrayList<THTNode>() );
        parent.getChildren().add( newNode );
        return newNode;
    }

    /**
     * Creates ArrayList of leaf nodes.
     * 
     * @return List of leaf THTNode references
     */
    public ArrayList<THTNode> getLeafNodes() {
        THTNode start = getRoot();
        ArrayList<THTNode> leafNodes = new ArrayList<THTNode>();
        return getLeafNodesR( start, leafNodes );
    }

    /**
     * Recursively builds ArrayList of leaf nodes
     * 
     * @param currentNode The current Node position in the tree
     * @param listRef A reference to the node list
     * @return A reference to the node list
     */
    private ArrayList<THTNode> getLeafNodesR(THTNode currentNode,
            ArrayList<THTNode> listRef) {
        // if node has children
        if ( currentNode.isLeaf() ) {
            listRef.add( currentNode );
            return listRef;
        }
        for ( THTNode n : currentNode.getChildren() ) {
            getLeafNodesR( n, listRef );
        }
        return listRef;
    }

    /**
     * Creates a structure for headers using a 2D ArrayList. Each entry in the
     * inner lists is a Label and column- / row-span corresponding to that
     * node's children.size()
     * 
     * @return 2-dimensional ArrayList where each entry in the outer list
     *         represents field parameters in the column or row field sets for
     *         the pivot table.
     */
    public ArrayList<ArrayList<THTLabel>> createHeaderLists() {
        THTNode start = this.getRoot();
        ArrayList<ArrayList<THTLabel>> headerLists = new ArrayList<>();
        for ( int i = 0; i <= this.getHeight(); i++ ) {
            headerLists.add( new ArrayList<THTLabel>() );
        }
        return createHeaderListsR( start, headerLists, 0 );
    }

    /**
     * Recursively builds the 2D ArrayList for the header lists. For each label
     * in the header, the span will be equal to the number of descendants, which
     * is calculated recursively.
     * 
     * @param currentNode Current position in the THT
     * @param headerLists The 2D ArrayList with the header information
     * @param depth The current depth in the THT
     * @return The 2D ArrayList with the header information
     */
    private ArrayList<ArrayList<THTLabel>> createHeaderListsR(THTNode currentNode,
            ArrayList<ArrayList<THTLabel>> headerLists, int depth) {

        // The recursive end condition: if node has children
        if ( currentNode.isLeaf() ) {
            // Since it's a leaf, it has a span of 1
            headerLists.get( depth )
                    .add( new THTLabel( currentNode.getElement().getFieldName(), 1 ) );
            return headerLists;
        }

        // If it's not a leaf node, the span is equal to the number of children
        int span = getDescendants( currentNode );

        headerLists.get( depth )
                .add( new THTLabel( currentNode.getElement().getFieldName(), span ) );

        // The recursive step
        for ( THTNode child : currentNode.getChildren() ) {
            createHeaderListsR( child, headerLists, depth + 1 );
        }

        return headerLists;
    }

    /**
     * Calculates the number of descendants for a given node TODO this algorithm
     * could probably be improved
     * 
     * @param currentNode
     * @return The number of descendants
     */
    public int getDescendants(THTNode currentNode) {
        if ( currentNode == this.getRoot() )
            return 0; // save processing since we're not using root
        return getDescendants( currentNode, 0 );
    }

    /**
     * Calculates the number of descendants recursively for a given node
     * 
     * @param currentNode
     * @param count The number of descendants
     * @return The number of descendants
     */
    private int getDescendants(THTNode currentNode, int count) {
        // Recursive end condition: if leaf node, return 1
        if ( currentNode.isLeaf() ) {
            return 1;
        }
        // If internal node, the number of descendants is the recursive total of
        for ( THTNode child : currentNode.getChildren() ) {
            count += getDescendants( child, 0 );
        }
        return count;
    }

    /* Getter and setter methods */

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

}
