package pivottable.dataimport;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import pivottable.io.CSVImporter;
import pivottable.model.PTData;
import pivottable.model.PTDataType;
import pivottable.model.PTField;
import pivottable.model.PivotTable;

public class CSVImporterTest {
	
	@Test
	public void has_valid_header_type() {
	  CSVImporter testImporter = CSVImporter.getInstance();
	  PivotTable pt = new PivotTable();
	  testImporter.importData("src/main/java/files/SalesJan2009.csv", pt);
	  PTData pivotData = pt.getPTData();
	  ArrayList<PTField> fields = pivotData.getFields(); 
	boolean allTrue = true; 
	  
    for (PTField field: fields){
    	if (!(field.getDataType() instanceof PTDataType)){
    		allTrue = false;
    		break;
    	}
    }
    
    assertTrue("All of the headerTypes should be instances of PTDataType" ,allTrue);
    
	}
}
