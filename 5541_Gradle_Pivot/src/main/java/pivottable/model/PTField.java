package pivottable.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Fields are a class with a fieldName and HashMap of String, HashSet K,V pairs
 */
public class PTField implements Serializable {

    /* For serializing */
    private static final long serialVersionUID = 726087760301569810L;
    
    /* Class attributes */

    /**
     * The field name
     */
    private String name;
    
    /**
     * The field label
     */
    private String label;
    
    /**
     * The datatype
     */
    private PTDataType datatype;

    /**
     * A HashMap of HashSet<Integer> representing the ids of the records, with
     * the field value as key
     */
    private HashMap<String, HashSet<Integer>> values;

    /* Constructor */

    /**
     * Default Constructor
     * 
     * @param name
     */
    public PTField( String name, PTDataType datatype ) {
        this.name = name;
        this.label = name;
        this.datatype = datatype;
        this.values = new HashMap<String, HashSet<Integer>>();
    }

    /**
     * Add a value to the field
     */
    public void addValue(String value) {
        this.values.put( value, new HashSet<Integer>() );
    }

    /**
     * Add a value to the field with HashSet
     */
    public void addValue(String value, HashSet<Integer> ids) {
        this.values.put( value, ids );
    }

    /**
     * Add id to value set
     */
    public void addValueId(String value, int id) {
        this.values.get( value ).add( id );
    }
    
    /* Getter and setter methods */

    /**
     * @return the field name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns ids for a given field value
     */
    public HashSet<Integer> getFieldIds(String value) {
        return this.values.get( value );
    }

    /**
     * Accessor for values
     */
    public HashMap<String, HashSet<Integer>> getValues() {
        return this.values;
    }

    /**
     * Return an alphabetical list of values
     */
    public ArrayList<String> getValuesAlpha(PTSortOrder sortOrder) {
        // instantiate arraylist
        ArrayList<String> valuesList = new ArrayList<String>();
        // generate the valuesList from the HashMap keys
        for ( String value : this.values.keySet() ) {
            valuesList.add( value );
        }
        // check sort order
        if (sortOrder == PTSortOrder.ASC) {
            // sort the keys by alpha
            Collections.sort( valuesList, new Comparator<String>() {
        
                @Override
                public int compare(String a, String b) {
                    return a.compareTo( b );
                }
            } );
        } else {
         // sort the keys by alpha desc
            Collections.sort( valuesList, new Comparator<String>() {
        
                @Override
                public int compare(String a, String b) {
                    return b.compareTo( a );
                }
            } );
        }
        
        // return the sorted list
        return valuesList;
    }

    /**
     * Accessor for field data type
     * @return PTDataType
     */
	public PTDataType getDataType() {
		return datatype;
	}
	
	/** 
	 * Sets datatype
	 * @param type
	 */
	public void setDataType(PTDataType type){
	    this.datatype = type;
	}

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}