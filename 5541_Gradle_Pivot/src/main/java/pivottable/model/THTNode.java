package pivottable.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * The THTNode is a simple node class used by the TableHeaderTree general tree.
 */
public class THTNode implements Serializable {

    /* For serializing */
    private static final long serialVersionUID = 7958335379098164063L;
    
    /* Class Attributes */

    private THTField element;
    private THTNode parent;
    private ArrayList<THTNode> children;

    /* Constructor */

    /**
     * The THTNode is a general-tree Node element for the TableHeaderTree
     * 
     * TODO should probably track descendants (children, grand-children, etc.)
     * for more efficient calculation of spans.
     * 
     * @param element The THTField for the element to store
     * @param parent A reference to the parent node
     * @param children A list of child nodes
     */
    public THTNode( THTField element, THTNode parent, ArrayList<THTNode> children ) {
        this.element = element;
        this.parent = parent;
        this.children = children;
    }

    /* Class Methods */

    public boolean isLeaf() {
        return children.isEmpty();
    }

    /**
     * Returns element ids
     * 
     * @return Set of element ids
     */
    public HashSet<Integer> getFieldIds() {
        return element.getFieldIds();
    }

    /* Getter and setter methods */

    public THTNode getParent() {
        return parent;
    }

    public void setParent(THTNode parent) {
        this.parent = parent;
    }

    public ArrayList<THTNode> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<THTNode> children) {
        this.children = children;
    }

    public THTField getElement() {
        return element;
    }

    public void setElement(THTField element) {
        this.element = element;
    }

}