package pivottable.ui;

/**
 * Interface for the PivotTable View
 */
public interface PTViewInterface {

    /**
     * Method with no parameters that draws full Pivot Table
     */
    public void drawPivotTable();

}
