package pivottable.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import pivottable.io.CSVImporter;
import pivottable.model.PTData;

public class PTCellTest {

	PTCell testCell;
	PivotTable testTable;
	PTData pivotData;

	
	@Before
	public void PTCellTest_setup() {
		//Import Data
		CSVImporter testImporter = CSVImporter.getInstance();
		PivotTable pt = new PivotTable();
		testImporter.importData("src/main/java/files/SalesJan2009.csv", pt );
		PTData pivotData = pt.getPTData();
		        
		ArrayList<PTFieldSet> columnFields = new ArrayList<>();
		ArrayList<PTFieldSet> rowFields = new ArrayList<>();
		
		PTFieldSet fs1 = new PTFieldSet(3, PTSortOrder.ASC); //payment
		PTFieldSet fs2 = new PTFieldSet(1, PTSortOrder.ASC); //product
		PTFieldSet fs3 = new PTFieldSet(7, PTSortOrder.ASC); //country
		PTFieldSet fs4 = new PTFieldSet(6, PTSortOrder.ASC); //state
		
		columnFields.add(fs1);
		columnFields.add(fs2);
		rowFields.add(fs3);
		rowFields.add(fs4);
			
		//2 = price ; 6 = columns per page; 20 = rows per page; 1 = current Page
	
		testTable = new PivotTable();
        testTable.setColumnFields( columnFields );
        testTable.setRowFields( rowFields );
        testTable.setAggregationField( 2 );
        testTable.setPTData( pivotData );
	}
	
	

	@Test
	public void test_PTCell_Maximum_Int_assertEquals() {
		//Q6:  Again, post calculation, how do we access a single cell (i.e. testCell).
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MAXIMUM);
		testCell.calculateValue();
		assertEquals("", "1200" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Maximum_Int_assertNotEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MAXIMUM);
		testCell.calculateValue();
		assertNotEquals("", "1201" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Minimum_Int_assertEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Arlington");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MINIMUM);
		testCell.calculateValue();
		assertEquals("", "800" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Minimum_Int_assertNotEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Arlington");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MINIMUM);
		testCell.calculateValue();
		assertNotEquals("", "801" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Sum_Int_assertEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.SUM);
		testCell.calculateValue();
		assertEquals("", "3600" ,testCell.getValue());	
	}
	
	

	@Test
	public void test_PTCell_Sum_Int_assertNotEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.SUM);
		testCell.calculateValue();
		assertNotEquals("", "3601" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Mean_Int_assertEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MEAN);
		testCell.calculateValue();
		assertEquals("", "1200.0" ,testCell.getValue());	
	}

	@Test
	public void test_PTCell_Mean_Int_assertNotEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.MEAN);
		testCell.calculateValue();
		assertNotEquals("", "1201.0" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Count_Int_assertEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.COUNT);
		testCell.calculateValue();
		assertEquals("", "3" ,testCell.getValue());	
	}

	@Test
	public void test_PTCell_Count_Int_assertNotEquals() {
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(5, "Anchorage");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.COUNT);
		testCell.calculateValue();
		assertNotEquals("", "2" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Count_String_assertEquals() {		
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(1, "Product1");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.COUNT);
		testCell.calculateValue();
		assertEquals("", "847" ,testCell.getValue());	
	}
	
	@Test
	public void test_PTCell_Count_String_assertNotEquals() {		
		HashSet<Integer> ids = testTable.getPTData().getFieldIdsByField(1, "Product1");
		testCell = PTCell.create(testTable, ids);
		testTable.setOperationType(PTOperation.COUNT);
		testCell.calculateValue();
		assertNotEquals("", "840" ,testCell.getValue());	
	}
}
